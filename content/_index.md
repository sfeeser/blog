---
title: "The House Stu Built" 
weight: 17
alwaysopen: true
collapsibleMenu: "false"
---


![image](/images/home-office2.webp)

Welcome to my blog, a space where I share the latest insights from my unique journey and the lessons learned along the way. Growing up as a contractor's son, I was introduced early to the world of building and creating, from neighborhoods to the intricate systems within homes. This upbringing taught me that it's the execution of ideas, not the ideas themselves, that are key. I was raised with a deep appreciation for people who can just get it done. I live to meet the challenge of turning concepts into reality, a principle I've aimed to apply beyond the construction site.

I remember my college friends talking about learning to fly, so of course, the next summer, while constructing a post-modern 4-bedroom house with my brother, I earned a private pilot's license. To this day, I think I just couldn't tolerate getting on a plane and not being able to drive. Clearly, the desire to drive still drives me.

Many years ago, and to my father's chagrin, I decided to become an engineer. I wanted to design and build electronic things, and Alcatel was the start of my professional path. At Alcatel, I was the engineer that the salespeople loved. I could explain things to them, and they would understand. I worked under a senior VP of marketing with an engineering background. His guidance was pivotal, not only in shaping my approach to work but also in solidifying my personal motto inspired by his encouragement: to face challenges with resilience, never giving up on pursuing solutions, no matter the odds. By 29, I was managing two product lines, which in retrospect, I have no idea why so much responsibility was assigned to someone so young. It still makes me laugh when think if the time when I sent a marketing spec to the engineers where I laid out their next product release. It was extremely detailed and very technical. They made me explain things to them that I was shocked they did not know. To this day, that has never changed. By the way, they LOVED me.

I started Alta3 Research, Inc., in 1997 and began taking on projects, mostly in training engineers, whom as I have said earlier, always liked the way I explain things. Early on, Alta3 Research became recognized as a training company, but at heart, I was, I am, and most likely will remain a design engineer and architect with a great attitude.

This blog is where I bring together my experiences from construction, aviation, the tech world, hoping to inspire and engage with others who share a passion for innovation and problem-solving. Here, I aim to explore the possibilities that lie in combining practical knowledge with an unwavering determination to achieve the seemingly impossible. If I am successful, then it will be you that says, "I have the strength of ten thousand Bengal tigers, and I never give up, and I never give up, and I NEVER GIVE UP."

