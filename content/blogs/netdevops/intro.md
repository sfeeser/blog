---
title: "Loading the Source of Truth"
date: 2024-02-22
draft: false
headingPost: "Author: Stu Feeser"
---


<video width="100%"  controls poster="/images/netdevops/Slide4.PNG">   >
  <source src="https://static.alta3.com/courses/mdd/webinar/demo-netdevops.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


Imagine managing a network with tens of thousands of devices. The mere thought 
of centralizing this data might seem overwhelming. You know that if you did have
a single source of truth and a testing network that was identical to your production
network, then these things become possible:
- Test changes on a network that is configured identically to your production network.
- Test rollbacks on a network that is configured identically to your production network.
- Rollback changes in seconds.
- Know the configuration of all your systems in REAL TIME.
- Convert, upgrade, or change configuration in seconds.
- Testing is conducted thoroughly and with complete peace of mind using Cisco Modeling Labs, not on your production network.

Yet, the cost of not doing so is far greater than you might expect.


Managing a large network, with its hundreds or even tens of thousands of devices, is daunting. 
The prospect of loading all that data into a single source of truth may seem too great a 
challenge, and you might be tempted to avoid it. However, the benefits of managing your
network with a single source of truth are undeniable. Here's how you can accomplish it.

### Can I load all my devices into a single source of truth?

Yes, you can. With just a few days of prep work, you can load the source of truth with firmware,
serial numbers, configuration files, and more, in just seconds per device. This process is 
error-free, powered by over a decade of cloud technology experience. Now, it's time to apply 
this to your network. Here is our step-by-step process:

### 1. Start with Your current network

![image](/images/netdevops/Slide1.PNG)

### 2. Install NetBox (minutes with our Ansible playbooks)


![image](/images/netdevops/Slide2.PNG)


### 3. Install Cisco Modeling Labs (CML) for testing. (minutes)

![image](/images/netdevops/Slide3.PNG)

### 4. Painlessly replicate your production network in CML.


![image](/images/netdevops/Slide4.PNG)


### 5. How we do it:

Through our Ansible playbooks, custom modules, and python scripts:
- Move your spreadsheet inventory into an Ansible inventory.
- Say goodbye to spreadsheets for managing your network!
- Transfer configuration information from your production network to NetBox in seconds.
- Move configuration files into GitLab for version control, changes, and rollbacks in seconds.
- Our playbooks ensure seamless integration and functionality across all these systems.
- Your entire network is backed up in GitLab and NetBox at the speed of thought.

![image](/images/netdevops/Slide5.PNG)


### 6. NetBox is now established as the Source of Truth. 

From NetBox, our Ansible playbooks will build a test network on CML


![image](/images/netdevops/Slide7.PNG)

### 7. The CML network is identical to the production network.
Go ahead and make any changes you need in the test network, without the stress and worry of otherwise using the production network as your testing platform!
![image](/images/netdevops/Slide8.PNG)

