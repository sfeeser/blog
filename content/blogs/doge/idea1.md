---
title: "Idea #1"
date: 2025-02-23
draft: false
weight: 100
headingPost: "Author: Stu Feeser"
---




**Stu's Idea #1:**

```
Could the US government seize the Fed by eminent domain and forgive government debt, issuing treasury bills to replace the classic dollar similar to what JFK was about to do?
```
**TL/DR GROK did not like this idea**


### GROK:
This is an intriguing hypothetical rooted in economics, law, and a dash of historical speculation. Let’s break it down.
The U.S. government seizing the Federal Reserve via eminent domain would be a wild move. Eminent domain allows the government to take private property for public use, provided there’s just compensation. The Fed, though, isn’t your typical private entity—it’s a weird hybrid. It’s a government-created system of central banks, independent in its operations but tied to public oversight through Congress and the Treasury. Its Board of Governors is appointed by the President and confirmed by the Senate, yet its 12 regional banks are technically owned by member banks. So, could the government "seize" it? Maybe, legally speaking, if it could argue the Fed’s private components are subject to eminent domain and that taking it serves a public purpose—like stabilizing the economy. Compensation would be a nightmare to figure out, though, given the Fed’s assets are mostly government securities and its profits already flow back to the Treasury.

Now, forgiving government debt held by the Fed is where it gets spicy. The Fed holds a big chunk of U.S. debt—about $7 trillion as of late 2024, roughly a quarter of the total. If the government took over the Fed, it could, in theory, cancel that debt since it’d be owed to itself. Poof—$7 trillion off the books. But here’s the rub: the Fed uses those Treasury securities as collateral to back the dollars it issues. Wiping them out without a replacement would tank confidence in the currency. Enter your idea of issuing Treasury bills to replace the dollar. This sounds like a pivot to a new currency system—ditching Fed-issued notes for something directly from the Treasury, echoing historical experiments like the Civil War-era Greenbacks.

That brings us to the JFK angle. There’s a persistent theory that Kennedy was planning something like this via Executive Order 11110 in 1963, which authorized the Treasury to issue silver certificates alongside Fed notes. Some claim he wanted to bypass the Fed entirely, but the evidence is shaky. EO 11110 didn’t abolish the Fed or forgive debt—it just let the Treasury issue limited notes backed by silver, and it was more about managing currency than upending the system. Plus, the Fed kept chugging along after his death, and those certificates were phased out by the late ‘60s as silver prices climbed. So, JFK’s move wasn’t quite the radical debt-canceling, Fed-seizing plot you might be imagining.

Could it work today? Mechanically, yes—the government could nationalize the Fed, cancel its own debt, and issue Treasury bills as a new currency. But the fallout would be chaos. Global markets would freak out—think dollar devaluation, soaring inflation, and a wrecked reputation for U.S. financial stability. Replacing the dollar isn’t like swapping out an old car; it’s the world’s reserve currency, baked into everything from oil trades to foreign reserves. Treasury bills as currency might stabilize things if they’re backed by something credible (gold? taxes?), but the transition would be a logistical and political mess. Congress would have to sign off, and good luck getting that through in today’s gridlock.

In short, it’s possible in theory but a Pandora’s box in practice. The Fed’s independence is a pain to some, but it’s also a buffer against political whims. JFK’s story is more legend than blueprint here—nothing suggests he was about to seize the Fed or erase debt. Still, it’s a fun thought experiment: what if the U.S. hit reset on its money printer?
