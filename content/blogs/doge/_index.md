---
title: "Groking the Economy" 
weight: 1
alwaysopen: true
collapsibleMenu: "true"
---

![image](/images/home-office.webp)

## Governmental Spending = Taxation

The key to a prosperous future is to reduce governental spending. If tax revenue falls short, then money must be borrowed, but we already redlined the debt, so borrowing no longer viable. The next solution is to print money. But that is even worse, because every new dollar printed causes inflation, which is the worst tax of all. I ran a few ideas into GROK and here is that conversation. 





