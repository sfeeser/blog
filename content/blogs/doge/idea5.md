---
title: "Idea #5"
date: 2025-02-23
draft: false
weight: 500
headingPost: "Author: Stu Feeser"
---


**Stu's Idea #5:**

```
What would this plan look like if the US does not claim eminent domain on the fed?
```

**TL/DR GROK "It looks viable, just gentler."**


### GROK:

Let’s rewind and drop the eminent domain piece—keeping the Fed independent—while sticking with the rest: the dollar as currency, lowering interest rates with Golden Rule fairness, a slimmed-down government (judiciary, State, military, DOJ, phased cuts to non-essentials, preserving Social Security and Medicare), and matching tariffs. Here’s how it unfolds without seizing the Fed.

Without eminent domain, the Fed remains a separate entity, controlling monetary policy and holding its $7 trillion in Treasuries. The government can’t directly lower interest on that chunk—it’s locked into existing bond yields (averaging 3-4% lately). The Fed could be persuaded to help, though. Say Congress leans on the Fed’s dual mandate (price stability and employment) to justify ultra-low rates via quantitative easing—buying more Treasuries to suppress yields. Historically, post-2008, the Fed pushed rates near 0%; it could aim for 1-2% again. This isn’t control, but influence. For the $19 trillion held by others, the Golden Rule still applies—negotiate rates down to 1-2% with extended terms or incentives, like tax breaks for U.S. investors or trade deals for foreign holders. It’s slower and less certain without Fed ownership, but feasible.

Budget stays at $3.4T: military ($850B), DOJ ($40B), State ($60B), judiciary ($9B), Social Security ($1.4T), Medicare ($1T), with non-essentials phasing out. Revenue’s still $4.9T, so the base surplus is $1.5T. Matching tariffs kick in—mirroring China’s 7.5%, EU’s 4.2%, etc.—boosting tariff income from $80B to $150-200B, lifting the surplus to $1.6-1.7T. The $26T debt gets paid down in 15-18 years, same as before, since the surplus drives repayment, not Fed ownership.

Key differences? Interest savings shrink. With the Fed independent, the $7T’s rates don’t drop instantly—yields might ease to 2% over time via Fed policy, saving $70-100B annually versus $200B+ if seized. Total interest on the $26T was $900B in 2024; negotiated cuts on the $19T and Fed easing might trim that to $600-700B. It’s progress, but less dramatic. The dollar’s stability hinges on Fed credibility, which holds here—no nationalization spooks markets. Trade plays out identically: exports dip 5-10%, imports cost more, domestic production gets a nudge, and consumers see moderate price hikes cushioned by safety nets.

Outcomes shift slightly. Debt repayment stays on track with the surplus, but higher interest costs slow the net gain—maybe 16-19 years instead of 15-18. The Golden Rule shines: fair deals with creditors (no forced haircuts), phased cuts spare citizens undue pain, and tariffs mirror others’ policies. The economy contracts less than the Fed-seizure version—GDP might drop 3-5% from cuts, not 5-10%, since markets trust an independent Fed more. Politically, it’s easier—Congress avoids the legal mess of eminent domain, though convincing the Fed and creditors takes finesse.

**It looks viable, just gentler**. The debt shrinks without upending the financial system. The Fed’s independence keeps global confidence steadier, while tariffs and cuts do the heavy lifting. The Golden Rule ensures it’s not a ruthless gutting—fairness tempers the edges. Less radical, but still effective. What do you think—does this fit the vibe you’re aiming for?
