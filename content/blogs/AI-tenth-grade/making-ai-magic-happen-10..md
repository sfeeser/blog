---
title: "Building AI magic"
date: 2024-02-22
draft: false
weight: 40
headingPost: "Author: Stu Feeser"
---

![image](/images/ai-magic.webp)

This blog entry breaks down some complex concepts about how artificial intelligence (AI) works into more understandable parts. Here's a simpler summary:

Imagine you're trying to make a super-smart computer program (like a really advanced Siri or Alexa) that can talk, think, or even write stories like a human. To do this, scientists use something called a transformer model, which isn't as scary as it sounds. It's basically a way to help computers understand and use language.

Here's what you need to make this AI "magic" happen:

1. **Training Data**: This is like the recipe book for AI. It needs lots of good examples to learn from. Just like in cooking, if you use bad ingredients (data), your result won't be good.

2. **Parameters**: Think of these as the brain cells of the AI. The more it has, the smarter it gets. When we reach a huge number, like 70 billion, the AI can start to understand and create language in ways that feel very human.

3. **Compute Power**: This is the kitchen where all the magic potion (AI) is brewed. The more powerful the kitchen (computer), the better the AI. We use special powerful computer parts called GPUs to make everything faster.

When you mix all these together for a few weeks or months, you get an AI model. This model can talk or write almost like a human because it has learned from a massive amount of data. But it doesn't just memorize everything; it really understands it, which is pretty cool.

**Learning How It Thinks**: There's a special process called backpropagation that helps the AI learn from its mistakes, making it smarter over time.

**Making Decisions**: The AI uses what it's learned to make guesses about what word comes next when it's talking or writing. This is called inference.

**Getting Help**: Sometimes, the AI needs a little help to make better guesses. That's where RAG comes in. It lets the AI look up extra information so it can understand better and give better answers.

**Understanding Its Decisions**: Explainable AI (XAI) is about making sure we can understand why the AI decides to say or write something. This helps make sure the AI stays on track and doesn't say something weird or wrong.

In conclusion, making AI is like cooking a very complex recipe. You need the right ingredients (data), a good kitchen (computer power), and a lot of patience to teach the AI how to "think". And just like in cooking, understanding every step of the process is key to making something amazing.

This blog is preparing you to understand some important terms about AI, like how it learns, thinks, and how we can make it as helpful as possible.