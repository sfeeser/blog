---
title: "Curating the Data"
date: 2024-02-22
draft: false
weight: 20
headingPost: "Author: Stu Feeser"
---


![image](/images/curating.webp)

### The Bridge Photo Project: Making a Million Photos Useful

Hey everyone! Today, let's dive into a cool project we're working on in our "Data to Dollars" adventure. Imagine we have a million photos of bridges. Yes, a million! These aren't just any photos. They could tell us a lot about the health of those bridges, like if there are cracks that need fixing or if they're in good shape. But here's the catch: not every photo is helpful. Some are too blurry, and some are just perfect. Our job is to sort them out and find the gems.

#### Making Photos Work for Us: A Step-by-Step Guide

Before we get into the nitty-gritty of sorting photos, let's get ready for what's coming. We'll talk about the money stuff in another blog, but for now, let's focus on the journey these photos will take. Not all will make it to the end, and that's okay. We're also going to see how much work each part takes—think of this as our guide to not getting lost in the photo mountain.

### 1. The Starting Line: Raw Photos
- **What It Is**: Imagine a giant pile of photos, some good, some bad, all mixed up. That's our starting point.
- **What Stays**: Everything! We haven't thrown anything out yet.
- **The Work**: About 5% of our time goes here. We're just getting everything set up, deciding what to do first.

### 2. Cleaning Up
- **What It Is**: Now we start sorting. We get rid of photos that are too messed up or don't help us at all.
- **What Stays**: Most of our photos, but we do say goodbye to some—around 80-90% make it through.
- **The Work**: This takes a bit more effort, about 15% of our time. It's like tidying up our messy room.

### 3. Getting Organized
- **What It Is**: With the photos we have left, we start putting them in order and making sense of them. We might group them by bridge or by what kind of problem they show.
- **What Stays**: We're picky here, keeping about 70-80% of what we had.
- **The Work**: This is a big chunk of our project, taking up 25% of our time. It's detailed work, like putting together a puzzle.

### 4. Adding More
- **What It Is**: Imagine taking some of the photos we have and creating new ones from them, like zooming in on a crack or showing it from a different angle.
- **What Stays**: It's not about getting rid of photos now; we're actually making more!
- **The Work**: About 20% of our effort goes here, adding variety to our collection.

### 5. Tagging Time
- **What It Is**: For the photos to be really useful, we need to label what’s in them, like "big crack" or "rusty spot."
- **What Stays**: We keep most of our collection, but let go of some that just don't fit, leaving us with 60-70%.
- **The Work**: Also 20% of our time. It's important to get the labels right, so we might need experts to help.

### 6. The Final Touch
- **What It Is**: We make our photos even better by adding extra info, maybe from other sources or by noting important details.
- **What Stays**: After all our sorting and adding, about half to a little over half of our original pile is super useful now.
- **The Work**: The last 15% of our effort makes sure everything is top-notch.

### Wrapping Up
Getting a million bridge photos ready is a huge job! If we use tools that aren't great, it's going to be even tougher. Making things easier for the team is super important. Good tools and smart AI can make a big difference, helping us focus on the really important stuff without getting bogged down.

And there you have it! We're turning a mountain of photos into valuable insights, one step at a time. Stay tuned for more on how we're making data work for us!