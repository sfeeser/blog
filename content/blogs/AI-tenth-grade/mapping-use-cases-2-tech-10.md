---
title: "Mapping models to use cases"
date: 2024-02-22
draft: false
weight: 30
headingPost: "Author: Stu Feeser"
---

![image](/images/ai-use-cases.webp)

**Intro**:
Hey everyone! Ever wonder how choosing different tech can make a big difference in traffic projects? Let’s dive into how some of the newest tech, especially these things called transformers, are changing the game compared to the old stuff. We’ll look at different traffic tasks and see which tech fits best.

### 1. **Watching Traffic Move**
- **Old Tech**: YOLOv4, Deep SORT
- **New Tech**: Vision Transformer (ViT)
- **Why It Matters**: Imagine ViT as having super vision that can see everything at once, unlike the old tech that can only focus on one part of the road. So, if a small car is hiding behind a big truck, ViT spots it while the old tech misses it. Plus, ViT learns faster because it gets the whole picture.

### 2. **Checking Road Health**
- **Old Tech**: Mask R-CNN, EfficientDet
- **New Tech**: DETR (DEtection TRansformer)
- **Why It Matters**: DETR is like having eagle eyes for the entire road, catching things out of place, like potholes, while ignoring stuff that doesn’t matter, like a fire hydrant off the road. Old tech struggles like someone walking around with a bad headache, missing the details.

### 3. **Reading License Plates**
- **Old Tech**: CRNN, Tesseract
- **New Tech**: Still the old ones!
- **Why It Matters**: Reading plates works better when you zoom in and block out distractions. Old tech does just that, focusing right on the plate.

### 4. **Telling Cars Apart**
- **Old Tech**: YOLOv4, EfficientNet
- **New Tech**: Vision Transformer (ViT)
- **Why It Matters**: ViT pays attention to tiny details, helping tell different car models apart, even ones that look almost the same. The old tech can’t keep up with ViT’s detail detective work.

### 5. **Counting People**
- **Old Tech**: Faster R-CNN, Deep SORT
- **New Tech**: DETR
- **Why It Matters**: DETR makes spotting and counting people in a crowd super simple, doing it all in one go. It’s like looking at a puzzle and seeing it already put together, while the old tech tries to fit each piece one by one.

### 6. **Finding Signs**
- **Old Tech**: RetinaNet, Tesseract OCR
- **New Tech**: DETR
- **Why It Matters**: DETR can spot all signs in a snap and then read them too. Old tech might miss some signs altogether.

### 7. **Watching Bridges**
- **Old Tech**: Mask R-CNN, EfficientNet
- **New Tech**: No new winner here
- **Why It Matters**: Keep an eye out! We need more work on using new tech to check bridges. Maybe looking for weird stuff directly is a smarter move.

### 8. **Spotting Weird Stuff**
- **Old Tech**: Autoencoders, GANs
- **New Tech**: Transformers for Anomaly Detection (TransAD)
- **Why It Matters**: TransAD takes in the whole scene and spots the odd bits way better than older tech, which gets confused by normal wear and tear.

### 9. **Watching Construction Sites**
- **Old Tech**: LSTM Networks, CNN-LSTM
- **New Tech**: Video Vision Transformer (ViViT)
- **Why It Matters**: Imagine being able to watch a whole construction project in an instant, seeing all changes over time clearly. ViViT does just that. So, no sneaky business when ViViT is on watch.

### 10. **Looking at Nature**
- **Old Tech**: U-Net, ResNet
- **New Tech**: SegFormer
- **Why It Matters**: SegFormer is like a nature expert, perfectly pointing out water, trees, and buildings in photos with pinpoint accuracy. It’s a pro at coloring within the lines, making it great for mapping out nature and cities.

### 11. **Finding Parking Spots**
- **Old Tech**: YOLOv4, CNNs
- **New Tech**: DETR
- **Why It Matters**: DETR can glance at a parking lot and instantly know where you can park. It’s a parking hero, spotting open spots fast without getting confused.

**Conclusion**:
Tech is always getting better, and for traffic projects, we’ve got some cool options. Keep an eye on:
- DETR for its eagle-eye views
- ViT for its super detail spotting
- TransAD for finding the odd stuff