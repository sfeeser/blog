---
title: "AI - Explain like I'm in 10th grade!" 
weight: 17
alwaysopen: false
collapsibleMenu: "false"
---

![image](/images/home-office.webp)

## Data to Dollars

"Data to Dollars" is like a guidebook I'm writing to help people who own businesses or work in them to make money from all the data they have just sitting around. Think of all the photos, texts, and numbers they've collected over the years. I want to show them how to use AI (Artificial Intelligence) to sort through that pile and find valuable stuff they can use.

My company, Alta3 Research, has seen a lot of different kinds of data, but everyone basically wants to do the same thing with it: use AI to make sense of it and do cool, smart things. The problem is, a lot of people don't know where to start. They hear about AI and think it sounds great, but they're not sure what's actually possible or how much it's going to cost. My goal is to clear up those questions, showing what you can really do with AI and how to get there without wasting time or money.

I also talk about the difference between being a scientist and an engineer in the AI world. Scientists are all about trying new things and figuring out what happens, while engineers make sure everything works smoothly and safely. In AI, you sometimes have to be a bit of both – trying new paths but also making sure the final result is something that works well. I'm here to guide people through this journey, switching hats from scientist to engineer, and making sure they get the hang of it. So, this is kind of a heads-up that we're going on an adventure to turn data into dollars.