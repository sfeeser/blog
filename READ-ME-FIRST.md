Make hugo work locally first. You will be glad that you did.
Here are the steps for WSL or a linux laptop:

```bash
cd ~

wget https://github.com/gohugoio/hugo/releases/download/v0.123.3/hugo_extended_0.123.3_linux-amd64.deb

sudo dpkg -i hugo_extended_0.123.3_linux-amd64.deb

rm hugo_extended_0.123.3_linux-amd64.deb

hugo new site quickstart

cd quickstart/

git init

git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke

echo "theme = 'ananke'" >> hugo.toml

hugo new posts/my-first-post.md

cat << EOF > content/posts/my-first-post.md
---
title: 'My First Post'
date: 2024-02-25T15:00:00-05:00
draft: true
---
## Introduction

This is **bold** text, and this is *emphasized* text.

Visit the [Alta3](https://alta3.com)
EOF

hugo server --buildDrafts
```


Now check this out. Make these changes:

cd into the "themes" directory

`wget https://github.com/McShelby/hugo-theme-relearn/archive/main.zip`

`sudo apt install unzip`

`unzip main.zip`

`rm main.zip`

cd back to the quickstart directory (cd ..) most likely

edit the hugo.toml file to look like this:

```
baseURL = 'https://example.org/'
languageCode = 'en-us'
title = 'Alta3 Research'
theme = 'hugo-theme-relearn-main'
```

Restart the server. The page will look GREAT
